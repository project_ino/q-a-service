import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        auth_user: ''
    },
    getters: {
        
    },
    mutations: {
        auth_user_data(state, user) {
            state.auth_user = user
        }      
    }
})