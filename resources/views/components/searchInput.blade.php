<form action="/search/{{ $type }}" method="get">
    <div class="input-group">
        <input type="text" class="form-control" name="q" placeholder="Search for...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </button>
        </span>
    </div>
</form>