<div class="question">
    <ul class="list-inline">
        @foreach ($question->tags as $tag)
            <li>
                <a href="/search/questions?tag={{ $tag->name }}" style="text-decoration: none;">
                    <span class="label label-default" style="font-weight: normal;">{{ $tag->name }}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <h3 class="question-subject"><a href="/question/{{ $question->id }}">{{ $question->subject }}</a></h3>
    <ul class="list-inline">
        <li>{{ $question->created_at->diffForHumans() }}</li>
        <li>Ответов: {{ $question->answers()->count() }}</li>
        <li>Автор: {{ $question->user->name }}</li>
    </ul>
</div>
<hr>