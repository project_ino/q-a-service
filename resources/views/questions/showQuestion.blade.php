@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::check() && Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-inline">
                        @foreach ($question->tags as $tag)
                            <li>
                                <a href="/search/questions?tag={{ $tag->name }}" style="text-decoration: none;">
                                    <span class="label label-default" style="font-weight: normal;">{{ $tag->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <h3>{{ $question->subject }}</h3>
                    @if (Auth::check() && Auth::user()->hasRole('administrator'))
                        <block-button :id="{{ $question->id }}" type="question"></block-button>
                    @endif
                    <p>{{ $question->body }}</p>
                    <div class="media">
                        <div class="media-left">
                            <a href="/user/{{ $question->user->id }}">
                                <img src="{{ asset('/avatars/' . $question->user->profile->avatar) }}" alt="" class="img-circle media-object" width="25px" height="25px">
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><a href="/user/{{ $question->user->id }}">{{ $question->user->name }}</a></h5>
                        </div>
                    </div>
                    <br>
                    
                    <hr>

                    <answer-feed :question-id="{{ $question->id }}"></answer-feed>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection