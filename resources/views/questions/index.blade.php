@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop



@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Теги</div>
                    <div class="panel-body">
                        <ul>
                            @foreach ($questions as $question )
                                @foreach ($question->tags as $tag)
                                    <li>{{ $tag->name  }}</li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                                </span>
                            </div>
                        </form>
                        <hr>
                        @foreach($questions as $question)
                        <div class="question">
                            @foreach ($question->tags as $tag)
                            <a>{{$tag->name }}</a>
                            @endforeach
                            <h3 class="question-subject"><a href="">{{ $question->body }}</a></h3>
                            <ul class="list-inline">
                                <li><a href="#">5 ответов</a></li>
                                <li>14.04.2017</li>
                                <li>Автор <a href="#" >{{ $question->user->name }}</a></li>
                            </ul>
                        </div>
                        @endforeach
                        <hr>
                        {{--<div class="question">--}}
                            {{--<a>Программмирование</a>--}}
                            {{--<h3 class="question-subject"><a href="">Lorem ipsum dolor.</a></h3>--}}
                            {{--<ul class="list-inline">--}}
                                {{--<li><a href="#">5 ответов</a></li>--}}
                                {{--<li>14.04.2017</li>--}}
                                {{--<li>3 подписчика</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection