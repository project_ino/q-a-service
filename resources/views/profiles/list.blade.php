@extends('layouts.app')

<style>
    .dropdown-item {
        color: #000080; /* Цвет обычной ссылки */
        padding: 2px; /* Поля вокруг текста */
    }
    .dropdown-item:visited {
        color: #800080; /* Цвет посещенной ссылки */
    }
    .dropdown-item:hover {
        text-decoration: none; /* Убираем подчеркивание */
        color: #fff; /* Цвет ссылки при наведении на нее курсора мыши */
        background: #e24486; /* Цвет фона */
    }
</style>

@section('navbar')
    <li><a href="/questions">Вопросы</a></li>
    <li><a href="profile/questions">Мои вопросы</a></li>
    <li><a href="profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Список пользователей</div>
                    <div class="panel-body">
                        @foreach($profiles as $profile)
                            <div class="dropdown show">
                                <a class="btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ $profile->user->name}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a class="dropdown-item" href="/profiles/{{ $profile->id }}">Просмотреть профиль</a></li>
                                        <li class="list-group-item"><a class="dropdown-item" href="/profiles/{{$profile->id}}/edit">Редактировать профиль</a></li>
                                        <li class="list-group-item"><a class="dropdown-item" href="#">Удалить пользователя</a></li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

