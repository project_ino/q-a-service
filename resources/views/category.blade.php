@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::check() && Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop


@section('content')
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                @include('components.searchInput', ['type' => $type])
            </div>
            <div class="panel-body">
                @foreach ($categoryItems as $categoryItem)
                    @includeWhen($type === 'questions', 'components.question', ['question' => $categoryItem])
                    @includeWhen($type === 'tags', 'components.tag', ['tag' => $categoryItem])
                    @includeWhen($type === 'users', 'components.user', ['user' => $categoryItem])
                @endforeach
                <center>{{ $categoryItems->links() }}</center>
            </div>
        </div>
    </div>
@endsection