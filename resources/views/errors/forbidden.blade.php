@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Ошибка</div>

                    <div class="panel-body">
                        У вас нет прав для просмотра этой страницы.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection