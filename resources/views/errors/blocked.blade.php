@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ошибка
            </div>
            <div class="panel-body">
                Вы заблокированы. Вы не можете создавать вопросы и ответы
            </div>
        </div>
    </div>
@endsection