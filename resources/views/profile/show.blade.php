@extends('layouts.app')

<style>
    li {
        list-style-type: none; /* Убираем маркеры */
    }
    ul {
        margin-left: 0; /* Отступ слева в браузере IE и Opera */
        padding-left: 0; /* Отступ слева в браузере Firefox, Safari, Chrome */
    }
</style>

@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $user->name}}

                    </div>
                    <div class="panel-body">
                        <img src="{{ asset('/avatars/' . $user->profile->avatar)  }}" alt="Profile Avatar"  class="avatar pull-right" class="avatar"  width="150">
                        <ul>
                            <li>Имя в системе: {{ $user->name }}</li>
                            <li>E-mail: {{ $user->email }}</li>
                            <li>Место жительства: {{ $user->profile->location }}</li>
                            <li>О себе: {{ $user->profile->about }}</li>
                            <li>Дата регистрации: {{ $user->created_at }}</li>
                        </ul>
                        <hr>
                        @if (Auth::user()->hasRole('administrator'))
                            <block-button class="pull-right" id="{{ $user->id }}" type="user"></block-button>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop