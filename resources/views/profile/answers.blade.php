@extends('layouts.app')

@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="/search" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                                </span>
                            </div>
                        </form>
                        @foreach ($answers as $answer)
                            <hr>
                            <div class="answer">
                                <h3 class="answer-subject">{{ $answer->content}}</h3>
                                <ul class="list-inline">
                                    <li>Вопрос: <a href="/question/{{$answer->question->id}}"> {{ $answer->question->subject }}</a> </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection