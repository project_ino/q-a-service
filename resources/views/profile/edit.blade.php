@extends('layouts.app')

@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Редактирование профиля</div>
                    <div class="panel-body">
                        <img src="{{ asset('/avatars/' . $user->profile->avatar)  }}" alt="Profile Avatar"  class="avatar center-block" class="avatar"  width="300">
                        {!! Form::open(['files' => 'true']) !!}
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="form-group">
                                {!! Form::label('location', 'Место жительства') !!}
                                {!! Form::text('location', $user->profile->location, ['class' => 'form-control',]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('about', 'О себе') !!}
                                {!! Form::textarea('about', $user->profile->about, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Зазрузите аватар') !!}
                                {!! Form::file('avatar') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Изменить профиль', ['class' => 'brn btn-primary form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::hidden('user_name', $user->name) !!}
                                {!! Form::hidden('user_id', $user->id) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        </div>
    </div>

@stop