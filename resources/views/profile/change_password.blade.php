@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Смена пароля</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('submit_change_password') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="old_password" class="col-md-4 control-label">Старый пароль</label>
                                <div class="col-md-6">
                                    <input id="old_password" type="password" class="form-control" name="old_password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Новый пароль</label>
                                <div class="col-md-6">
                                    <input id="new_password" type="password" class="form-control" name="new_password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="confirm_password" class="col-md-4 control-label">Новый пароль</label>
                                <div class="col-md-6">
                                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Сменить пароль
                                    </button>
                                </div>
                            </div>
                        </form>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop