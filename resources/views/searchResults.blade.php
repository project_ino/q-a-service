@extends('layouts.app')


@section('navbar')
    <li><a href="/profile/questions">Мои вопросы</a></li>
    <li><a href="/profile/answers">Мои ответы</a></li>
    @if (Auth::user()->hasRole('administrator'))
        <li><a href="/profiles">Список пользователей</a></li>
    @endif
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                
                <div class="panel-heading">
                    @include('components.searchInput', ['type' => $type])
                </div>
                
                <div class="panel-body">
                    <p>Результаты поиска:</p>
                    <hr>
                    @foreach ($results as $result)
                        @includeWhen($type === 'questions', 'components.question', ['question' => $result])
                        @includeWhen($type === 'tags', 'components.tag', ['tag' => $result])
                        @includeWhen($type === 'users', 'components.user', ['user' => $result])
                    @endforeach
                    <center>{{ $results->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
