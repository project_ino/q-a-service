@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="/create/question" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="subject">Суть вопроса</label>
                        <input type="text" id="subject" name="subject" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="tags">Теги</label>
                        <select class="form-control" multiple name="tags[]">
                            @foreach (App\Tag::all() as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="details">Детали вопроса</label>
                        <textarea name="details" id="details" rows="5" class="form-control"></textarea>
                    </div>
                    <button class="btn btn-primary">Создать</button>
                </form>
            </div>
        </div>
    </div>
@endsection