<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'QuestionController@index')->name('main');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Изменение пароля
Route::get('/profile/change_password', 'ChangePasswordController@getChange')->name('change_password');
//Изменение профиля
Route::get('/profile/edit', 'ProfileController@getEdit')->name('edit_profile');
//Страница профиля
Route::get('/profile', 'ProfileController@index')->name('profile');
//Страница "Мои вопросы"
Route::get('/profile/questions', 'ProfileController@getUserQuestions')->name('profile.questions');
//Страница "Мои ответы"
Route::get('/profile/answers', 'ProfileController@getUserAnswers');

//Обработка изменений в профиле
Route::post('/profile/edit', 'ProfileController@postEdit')->name('submit_edit_profile');
//Обработка измнения пароля
Route::post('/profile/change_password', ['middleware' => 'check-pass', 'uses' => 'ChangePasswordController@postChange'])->name('submit_change_password');


//Роуты для АДМИНА
Route::group(['middleware' => 'check-roles:administrator'], function ()
{
//Редактирование выбранного пользователя
    Route::get('/profiles/{id}/edit', ['as' => 'edit_any_profile', 'uses' => 'ProfileController@getEditUserProfile'])->where('id', '[0-9]+');
//Просмотр чужого профиля`
    Route::get('/profiles/{id}', ['as' => 'show_profile', 'uses' => 'ProfileController@show'])->where('id', '[0-9]+');
//Страница со списком профилей в системе
    Route::get('/profiles', 'ProfileController@getProfiles')->name('profiles');

//Обработка изменений в чужом профиле
    Route::post('/profiles/{id}/edit','ProfileController@postEditUserProfile')->where('id', '[0-9]+');

});


Route::get('/search', 'QuestionController@search');

Route::get('/questions', 'QuestionController@index')->name('questions.index');
Route::get('/tags', 'TagController@index')->name('tags.index');
Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/search/{type}', 'SearchController@search');

Route::get('/question/{id}', 'QuestionController@show')->name('question.select');
Route::get('/user/{id}', 'ProfileController@show');

Route::get('/answer/like/{id}', 'LikeController@like');
Route::get('/answer/unlike/{id}', 'LikeController@unlike');
Route::get('/answer/is_user_liked/{id}', 'LikeController@isUserLiked');
Route::get('/answer/likes/count/{id}', 'LikeController@likesCount');


Route::middleware(['check-blocked', 'auth'])->group(function () {
    Route::match(['get', 'post'], '/create/question', 'QuestionController@create')->name('questions.create');
    Route::post('/create/answer', 'AnswerController@create')->name('answers.create');
});


Route::get('/block/user/{id}', 'BlockController@addUser');
Route::get('/block/question/{id}', 'BlockController@addQuestion');
Route::get('/block/answer/{id}', 'BlockController@addAnswer');
Route::get('/unblock/user/{id}', 'BlockController@removeUser');
Route::get('/unblock/question/{id}', 'BlockController@removeQuestion');
Route::get('/unblock/answer/{id}', 'BlockController@removeAnswer');
Route::get('/is_blocked/user/{id?}', 'BlockController@isBlockedUser');
Route::get('/is_blocked/question/{id}', 'BlockController@isBlockedQuestion');
Route::get('/is_blocked/answer/{id}', 'BlockController@isBlockedAnswer');

Route::get('/get_user_auth_data', function () {
    if (Auth::check())
        return Auth::user()->load('roles');
    else
        return null;
});

Route::get('/answers/{questionId}', 'AnswerController@get');
Route::get('/answer/delete/{id}', 'AnswerController@delete');