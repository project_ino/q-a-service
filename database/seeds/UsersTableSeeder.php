<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_administrator = Role::where('name', 'administrator')->first();
        $role_member = Role::where('name', 'member')->first();

        //$profile = new \App\Profile();

        $administrator = new User();
        $administrator->name = 'Administrator Name';
        $administrator->email = 'administrator@example.com';
        $administrator->password = bcrypt('test1234');
        $administrator->save();
        $administrator->roles()->attach($role_administrator);
        $administrator->roles()->attach($role_member);
        $administrator->profile()->save(factory(App\Profile::class)->make());

        $member = new User();
        $member->name = 'Member Name';
        $member->email = 'member@example.com';
        $member->password = bcrypt('test1234');
        $member->save();
        $member->roles()->attach($role_member);
        $member->profile()->save(
            factory(App\Profile::class)->make()
        );

        factory('App\User', 20)->create()->each(
            function ($user) {
                $user->roles()->attach(2);

                $user->profile()->save(
                        factory(App\Profile::class)->make()
                );   
            });
    }
}
