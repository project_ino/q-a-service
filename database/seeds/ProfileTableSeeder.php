<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator_profile = new Profile();
        $administrator_profile->user_id = '1';
        $administrator_profile->address = 'Chekhova 175A';
        $administrator_profile->phone = '89185051122';
        $administrator_profile->save();

        $member_profile = new Profile();
        $member_profile->user_id = '2';
        $member_profile->address = 'Smirnovskogo 175A';
        $member_profile->phone = '8928178123321';
        $member_profile->save();
    }
}
