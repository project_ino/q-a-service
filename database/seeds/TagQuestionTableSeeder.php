<?php

use Illuminate\Database\Seeder;

class TagQuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(App\Question::all() as $question)
        {
            for ($i = 0; $i < rand(1, 5); $i++)
            {
                $question->tags()->attach(rand(1, 10));
            }
        }
    }
}