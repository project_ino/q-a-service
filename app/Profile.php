<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'location', 'about', 'avatar'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function updateInfo($data)
    {
        $profile = Profile::where('user_id', $data['user_id']);

        $profile->update([
            'location' => $data['location'],
            'about' => $data['about'],
            'avatar' => $data['avatar'],
        ]);

    }
}
