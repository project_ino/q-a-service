<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'subject', 'body', 'user_id', 'question_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'tag_question');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function answersCount()
    {
        return $this->answers()->count();
    }
}
