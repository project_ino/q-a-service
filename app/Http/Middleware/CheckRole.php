<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $guard = null)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }
        $user = Auth::user();

        if (!$user->hasRole($role)) {
            return \response(view('errors.forbidden'), 403);
        }
        return $next($request);
    }
}
