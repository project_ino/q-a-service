<?php

namespace App\Http\Middleware;

use Closure;

class ChangePassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::user()) {
            return \response(view('auth.login'));
        }

        $data = $request->all();

        $user = Auth::user()->getAuthPassword();

        if(!password_verify($data['old_password'], $user))

            return redirect()->back()->withErrors('Указан неверный пароль');

        return $next($request);
    }
}
