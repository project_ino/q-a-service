<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\BlackUser;

class CheckBlock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = BlackUser::where('user_id', Auth::id())->first();
        if (!is_null($user)) {
            return \response(view('errors.blocked'), 403);
        }
        
        return $next($request);

    }
}
