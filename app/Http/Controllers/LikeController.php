<?php

namespace App\Http\Controllers;

use App\Like;
use Auth;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function like($id)
    {
        $like = new Like;
        $like->user_id = Auth::id();
        $like->answer_id = $id;
        $like->save();

        if (!is_null($like)) {
            return 1;
        } else {
            return 0;
        }

        return $like;
    }

    public function unlike($id)
    {
        $like = Like::where('user_id', Auth::id())
             ->where('answer_id', $id)
             ->first();

        $like->delete();
    }

    public function isUserLiked($id)
    {
        if (Auth::check()) {
            $like = Like::where('user_id', Auth::id())
                        ->where('answer_id', $id)
                        ->first();

            if (is_null($like)) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 0;
        }
    }

    public function likesCount($id)
    {
        return Like::where('answer_id', $id)
                    ->count();
    }
}
