<?php

namespace App\Http\Controllers;

use App\Question;
use Auth;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        return view('category', [
            'categoryItems' => Question::paginate(10),
            'type' => 'questions'
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('createQuestion');
        } else {
            $question = new Question;
            $question->subject = $request->subject;
            $question->body = $request->details;
            $question->user_id = Auth::id();
            $question->save();
            foreach ($request->tags as $tag) {
                $question->tags()->attach($tag);
            }
            

            return redirect()->route('question.select', ['id' => $question->id]);
        }
        
    }

    public function show($id)
    {
        return view('questions.showQuestion', ['question' => Question::find($id)]);
    }


    public function answersCount($id)
    {
        
    }

}
