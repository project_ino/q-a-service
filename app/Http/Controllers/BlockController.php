<?php

namespace App\Http\Controllers;

use App\BlackUser;
use App\BlackQuestion;
use App\BlackAnswer;
use Auth;
use App\Illuminate\Http\Request;

class BlockController extends Controller
{
    public function addUser($id)
    {
        $user = new BlackUser;
        $user->user_id = $id;
        $user->save();

        return response()->json($user);
    }

    public function addQuestion($id)
    {
        $question = new BlackQuestion;
        $question->question_id = $id;
        $question->save();

        return response()->json($question);
    }

    public function addAnswer($id)
    {
        $answer = new BlackAnswer;
        $answer->answer_id = $id;
        $answer->save();

        return response()->json($answer);
    }

    public function removeUser($id)
    {
        $user = BlackUser::where('user_id', $id)
                                 ->first()
                                 ->delete();
        return response()->json($user);
    }

    public function removeQuestion($id)
    {
        $question = BlackQuestion::where('question_id', $id)
                                 ->first()
                                 ->delete();
        return response()->json($question);
    }

    public function removeAnswer($id)
    {
        $answer = BlackAnswer::where('answer_id', $id)
                                 ->first()
                                 ->delete();
        return response()->json($answer);
    }

    public function isBlockedUser($id)
    {
        if ($id === 'authenticated')
            $id = Auth::id();
        $user = BlackUser::where('user_id', $id)
                         ->first();
        if ($user)
            return 1;
        else
            return 0;
    }

    public function isBlockedQuestion($id)
    {
        $question = BlackQuestion::where('question_id', $id)
                         ->first();
        if ($question)
            return 1;
        else
            return 0;
    }

    public function isBlockedAnswer($id)
    {
        $answer = BlackAnswer::where('answer_id', $id)
                         ->first();
        if ($answer)
            return 1;
        else
            return 0;
    }
}
