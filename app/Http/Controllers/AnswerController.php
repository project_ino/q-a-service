<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function create(Request $request)
    {
        $answer = new Answer;
        $answer->user_id = Auth::id();
        $answer->question_id = $request->input('question_id');
        $answer->content = $request->input('content');
        $answer->save();

        return $answer->load('user');
    }

    public function get($questionId) {
        return Answer::where('question_id', $questionId)->get()->all();
    }

    public function delete($id)
    {
        $answer = Answer::where('id', $id)
                        ->first()
                        ->delete();

        return response()->json($answer);
    }
}
