<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{
    public function getChange()
    {
        $user = Auth::user();

        return view('profile.change_password',['user' => $user]);
    }
    public function postChange(ChangePasswordRequest $request)
    {
        $user = Auth::user();

        $data = $request->all();

        $user->password = bcrypt($data['new_password']);
        $user->save();

        return redirect('/profile');

    }
}
