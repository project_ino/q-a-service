<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        return view('category', [
            'categoryItems' => Tag::paginate(10),
            'type' => 'tags'
        ]);
    }
}
