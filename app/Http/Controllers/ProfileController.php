<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditProfileRequest;
use App\Question;
use App\Answer;
use App\User;
use App\Profile;

class ProfileController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');

    }
    //Информация о выбранном пользователе, только для Админа
    public function show($id)
    {
        if(!is_null($user = User::find($id))) {

            return view('profile.show', ['user' => $user]);

        } else {

            return view('404');
        }

    }
    //Информация об авторизированном пользователе
    public function index()
    {
        $user = \Auth::user();

        return view('profile.show', ['user' => $user]);
    }

    public function getEdit()
    {
        $user = \Auth::user();


        return view('profile.edit', ['user' => $user]);
    }
    //Изменение информации авторизированного пользователя
    public function postEdit(EditProfileRequest $request)
    {
        $user = \Auth::user();
        $data = $request->all();

        if($request->hasFile('avatar')) {

            $input_file = $request->file('avatar');

            $file_name = $request->user_name . time() . '.' . $input_file->getClientOriginalExtension();

            $input_file->move(public_path('\avatars\\'), $file_name);

            $data['avatar'] = $file_name;

        } else {
            $data['avatar'] = 'default.jpg';
        }

        $profile = new Profile();
        $profile->updateInfo($data);

        return redirect()->route('profile',['user' => $user]);

    }

    public function getProfiles()
    {
        $profiles = Profile::all();

        return view('profiles.list', ['profiles' => $profiles]);
    }
    //Измнения информации выбранного пользователя, только для Админа
    public function getEditUserProfile($id)
    {
        $user = User::find($id);

        if(!is_null($user))
        {
            return view('profiles.edit')->with('user', $user);
        } else {
            return abort(404);
        }
    }
    //Изменение информации о выбранном пользователе, только для Админа
    public function postEditUserProfile(EditProfileRequest $request)
    {
        $data = $request->all();

        $profile = new Profile();

        if($request->hasFile('avatar')) {

            $input_file = $request->file('avatar');

            $file_name = $request->user_name . time() . '.' . $input_file->getClientOriginalExtension();

            $input_file->move(public_path('\avatars\\'), $file_name);

            $data['avatar'] = $file_name;
        } else {
            $data['avatar'] = 'default.png';
        }

        $profile->updateInfo($data);

        return redirect()->route('show_profile', ['user' => $profile, 'id' => $profile->id]);
    }
    //Список вопросов авторизированного пользователя
    public function getUserQuestions()
    {
        $user = \Auth::user();
        $questions = Question::where('user_id', $user->id)->get();

        return view('profile.questions', ['questions' => $questions]);
    }
    //Список ответов авторизированного пользователя
    public function getUserAnswers()
    {
        $user = \Auth::user();
        $answers = Answer::where('user_id', $user->id)->get();

        return view('profile.answers' , ['answers' => $answers]);
    }

}
