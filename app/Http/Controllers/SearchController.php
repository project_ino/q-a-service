<?php

namespace App\Http\Controllers;

use App\Question;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search($type, Request $request)
    {
        $query = $request->q;
        switch($type)
        {
            case 'questions':
                if ($request->tag)
                {
                    $results = Tag::where('name', $request->tag)
                                  ->first()
                                  ->questions()
                                  ->paginate(10);
                }
                else
                {
                    $results = Question::where('subject', 'like', "%{$query}%")
                                       ->paginate(10);
                }
                
                break;
            case 'tags':
                $results = Tag::where('name', 'like', "%{$query}%")->paginate(10);
                break;
            case 'users':
                $results = User::where('name', 'like', "%{$query}%")->paginate(10);
                break;
        }   

        return view('searchResults', ['type' => $type, 'results' => $results]);
    }
}
