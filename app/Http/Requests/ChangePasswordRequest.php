<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'string|max:255',
            'new_password' => 'string|min:6|max:255',
            'password_confirmation' => 'string|min:6|max:255|same:password',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute необходимо заполнить',
            'min' => 'Поле :attribute должно содержать :min символов',
            'string' => 'Поле :attribute должно быть строкой',
            'max' => 'Поле :attribute не должно првевышать :max символов',
            'same' => 'Поля :attribute и :other должы совпадать'
        ];
    }
}
