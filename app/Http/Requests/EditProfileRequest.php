<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location' => 'string',
            'about' => 'string',
            'avatar' => 'image',
        ];
    }

    public function messages()
    {
        return [
            'string' => 'Поле :attribute должнл быть строкой',
            'min' => 'Поле :attribute должно содержать минимум :min символов',
            'image' => 'Загруженный файл должен быть изображением',
        ];
    }
}
