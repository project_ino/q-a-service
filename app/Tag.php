<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'tag_question');
    }


    public function getListTags()
    {
        $tags = Tag::all()->toArray();
        return $tags;
    }
}
