## Как развернуть на OpenServer

* Скачать репозиторий через git clone. Например, если через git bash: git clone https://barkhat@bitbucket.org/project_ino/kitchen-online.git kitchen.dev
* В терминале, который идет с OpenServer, перейти в папку с репозиторием с помощью cd domains\<приложение>
* В терминале выполнить: composer install
* В терминале выполнить: copy .env.example .env
* В терминале выполнить: php artisan key:generate
* Открыть ваш .env файл и изменить имя БД DB_DATABASE, пользователя БД DB_USERNAME и пароль к БД DB_PASSWORD. Eсли БД нет, то создаем ее через phpMyAdmin, который идет вместе с OpenServer, пользователь - "root", пароль - пустой
* В терминале выполнить: php artisan migrate
* В настройках OpenServer на вкладке Домены настроить путь, указав в качестве папки домена <приложение>/public